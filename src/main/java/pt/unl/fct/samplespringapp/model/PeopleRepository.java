package pt.unl.fct.samplespringapp.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PeopleRepository extends CrudRepository<Person,Long>
{
    Person findByName(String name);

    @Query("SELECT p FROM Person p WHERE p.name LIKE CONCAT('%',:name,'%')")
    Iterable<Person> searchByName(@Param(value = "name") String name);

}
