package pt.unl.fct.samplespringapp.security;

import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import pt.unl.fct.samplespringapp.model.PeopleRepository;
import pt.unl.fct.samplespringapp.model.Person;
import pt.unl.fct.samplespringapp.model.Pet;
import pt.unl.fct.samplespringapp.model.PetRepository;

import java.util.Optional;

@Service
public class mySecurityService {

    private PeopleRepository people;
    private PetRepository pets;

    public mySecurityService(PeopleRepository people, PetRepository pets) {
        this.people = people;
        this.pets = pets;
    }

    public boolean isPrincipal(User user, long id) {
        Optional<Person> person = people.findById(id);
        return person.isPresent() && person.get().getName().equals(user.getUsername());
    }

    public boolean isPetOwner(long id, long pid) {
        Optional<Person> person = people.findById(id);
        Optional<Pet> pet = pets.findById(pid);
        return person.isPresent() && pet.isPresent() && pet.get().getOwner().equals(person.get());
    }
}
