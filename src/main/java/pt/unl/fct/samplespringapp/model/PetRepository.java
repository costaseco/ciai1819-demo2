package pt.unl.fct.samplespringapp.model;

import org.springframework.data.repository.CrudRepository;

public interface PetRepository extends CrudRepository<Pet,Long> {
}
