package pt.unl.fct.samplespringapp.controllers;

import org.springframework.web.bind.annotation.*;
import pt.unl.fct.samplespringapp.errors.BadRequestException;
import pt.unl.fct.samplespringapp.errors.NotFoundException;
import pt.unl.fct.samplespringapp.model.PeopleRepository;
import pt.unl.fct.samplespringapp.model.Person;
import pt.unl.fct.samplespringapp.model.Pet;
import pt.unl.fct.samplespringapp.model.PetRepository;
import pt.unl.fct.samplespringapp.security.CanDeletePetsOfPerson;
import pt.unl.fct.samplespringapp.security.CanModifyPetsOfPerson;
import pt.unl.fct.samplespringapp.security.CanReadPetsOfPerson;

import java.util.Optional;

@RestController
@RequestMapping("/people")
public class PeopleController {

    private final PeopleRepository people;
    private final PetRepository pets;

    public PeopleController(PeopleRepository people, PetRepository pets) {
        this.people = people;
        this.pets = pets;
    }

    @GetMapping("")
    Iterable<Person> getAllPersons(
            @RequestParam(required = false) String search) {
        if( search == null )
            return people.findAll();
        else
            return people.searchByName(search);
    }

    @PostMapping("")
    void addNewPerson(@RequestBody Person p) {
        people.save(p);
    }

    @GetMapping("{id}")
    Person getOne(@PathVariable long id) {
        Optional<Person> maybe = people.findById(id);
        if( maybe.isPresent() )
            return maybe.get();
        else
            throw new NotFoundException(
                    String.format("Person with id %d does not exist", id));
    }

    @PutMapping("{id}")
    void updatePerson(@PathVariable long id, @RequestBody Person p) {
        if( p.getId() == id) {
            Optional<Person> old_p = people.findById(id);
            if( old_p.isPresent() ) {
                people.save(p);
            }
            else
                throw new NotFoundException(
                        String.format("Person with id %d does not exist", id));
        } else
            throw new BadRequestException("invalid request");
    }

    @DeleteMapping("{id}")
    void deletePerson(@PathVariable long id) {
        Optional<Person> old_p = people.findById(id);
        if( old_p.isPresent() ) {
            people.delete(old_p.get());
        } else
            throw new NotFoundException(
                    String.format("Person with id %d does not exist", id));
    }

    @GetMapping("{id}/pets")
    @CanReadPetsOfPerson
    Iterable<Pet> getAllPets(@PathVariable long id) {
        Optional<Person> maybePerson = people.findById(id);
        if( maybePerson.isPresent() ) {
            return maybePerson.get().getPets();
        } else
            throw new NotFoundException(
                    String.format("Person with id %d does not exist", id));
    }

    @PostMapping("{id}/pets")
    @CanModifyPetsOfPerson
    void addNewPet(@PathVariable long id, @RequestBody Pet pet) {
        Optional<Person> maybePerson = people.findById(id);
        if( maybePerson.isPresent() ) {
            Person person = maybePerson.get();
            pet.setOwner(person);
            pets.save(pet);
        } else
            throw new NotFoundException(
                    String.format("Person with id %d does not exist", id));
    }

    @DeleteMapping("{id}/pets/{pid}")
    @CanDeletePetsOfPerson
    void deletePet(@PathVariable long id, @PathVariable long pid) {
        Optional<Person> maybePerson = people.findById(id);
        if( maybePerson.isPresent() ) {
            Person person = maybePerson.get();
            Optional<Pet> maybePet = pets.findById(pid);
            if( maybePet.isPresent() ) {
                Pet pet = maybePet.get();
                if( pet.getOwner() == person ) {
                    pets.delete(pet);
                } else
                    throw new NotFoundException("Pet with id " + pid + " not found.");
            }
            else
                throw new BadRequestException("Pet with id "+pid+" does not belong to Person with id "+id);
        } else
            throw new NotFoundException(
                    String.format("Person with id %d does not exist", id));
    }
}
