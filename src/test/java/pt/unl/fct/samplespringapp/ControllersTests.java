package pt.unl.fct.samplespringapp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pt.unl.fct.samplespringapp.model.PeopleRepository;
import pt.unl.fct.samplespringapp.model.Person;
import pt.unl.fct.samplespringapp.model.Pet;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ControllersTests {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private PeopleRepository people;

    private MockMvc mockMvc;

    @Before
    public void init() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();

        // Data Fixture
        people.deleteAll();
        people.save(new Person(0, "John", "password"));
        people.save(new Person(0, "Mary", "password"));
    }

    private List<Person> getPeople() throws Exception {
        final MvcResult result = this.mockMvc.perform(get("/people"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andReturn();
        ;

        String list = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(list,new TypeReference<List<Person>>(){});
    }

    @Test
    public void testGetRecords() throws Exception {
        List<Person> people = getPeople();
        assertTrue(people.stream().anyMatch( (p) -> p.getName().equals("John")));
        assertTrue(people.stream().anyMatch( (p) -> p.getName().equals("Mary")));
    }


    @Test
    public void addPets() throws Exception {

        List<Person> people = getPeople();
        long firstId = people.get(0).getId();

        this.mockMvc.perform(post("/people/"+firstId+"/pets")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"id\":\"0\", \"name\":\"Bobby\"}"))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/people/"+firstId+"/pets"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$",hasSize(1)))
                .andExpect(jsonPath("$[0].name", is("Bobby")));
    }

    @Test
    public void addRemovePets() throws Exception {

        List<Person> people = getPeople();
        long firstId = people.get(0).getId();

        ObjectMapper mapper = new ObjectMapper();

        Pet pet = new Pet("pio");
        String json = mapper.writeValueAsString(pet);

        this.mockMvc.perform(post("/people/"+firstId+"/pets")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isOk())
                .andReturn();

        final MvcResult result = this.mockMvc.perform(get("/people/" + firstId + "/pets"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        String list = result.getResponse().getContentAsString();

        List<Pet> pets = mapper.readValue(list,new TypeReference<List<Pet>>(){});
        long firstPetId = pets.get(0).getId();

        this.mockMvc.perform(delete("/people/"+firstId+"/pets/"+firstPetId))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/people/"+firstId+"/pets/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$",hasSize(0)));
    }
}
