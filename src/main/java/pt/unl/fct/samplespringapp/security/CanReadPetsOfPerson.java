package pt.unl.fct.samplespringapp.security;

import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@PreAuthorize(CanModifyPetsOfPerson.Condition)
public @interface CanReadPetsOfPerson {
}
