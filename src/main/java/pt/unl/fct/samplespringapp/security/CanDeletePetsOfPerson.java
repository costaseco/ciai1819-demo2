package pt.unl.fct.samplespringapp.security;

import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@PreAuthorize(CanDeletePetsOfPerson.Condition)
public @interface CanDeletePetsOfPerson {
    String Condition =
            "hasRole('ADMIN') or " +
            "@mySecurityService.isPrincipal(principal, #id) and " +
            "@mySecurityService.isPetOwner(#id, #pid)";
}
