package pt.unl.fct.samplespringapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;
import pt.unl.fct.samplespringapp.model.PeopleRepository;
import pt.unl.fct.samplespringapp.model.Person;
import pt.unl.fct.samplespringapp.model.Pet;
import pt.unl.fct.samplespringapp.model.PetRepository;

import javax.transaction.Transactional;

@SpringBootApplication
public class SampleSpringAppApplication implements CommandLineRunner {

    public static void main(String[] args) {

        SpringApplication.run(SampleSpringAppApplication.class, args);
    }

    @Autowired
    PeopleRepository people;

    @Autowired
    PetRepository pets;

    @Autowired
    PasswordEncoder encoder;

    @Override
    @Transactional
    public void run(String... args) throws Exception {

        people.save(new Person(0, "John", encoder.encode("password")));
        people.save(new Person(0, "Mary", encoder.encode("password")));

    }
}
